class AddAnhaengeToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :anhaenge, :string
    add_column :articles, :hauptanhang, :string
  end
end
