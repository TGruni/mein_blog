class CreateArticlesAttachments < ActiveRecord::Migration
  def change
    create_table :articles_attachments, :id => false do |t|
      t.references :article, index: true, foreign_key: true
      t.references :attachment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
