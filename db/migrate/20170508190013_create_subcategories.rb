class CreateSubcategories < ActiveRecord::Migration
  def change
    create_table :subcategories do |t|
      t.references :category, index: true, foreign_key: true
      t.string :left
      t.text :right

      t.timestamps null: false
    end
  end
end
