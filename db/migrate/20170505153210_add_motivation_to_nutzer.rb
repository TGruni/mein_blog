class AddMotivationToNutzer < ActiveRecord::Migration
  def change
    add_column :nutzers, :motivation, :text
  end
end
