Nutzer.create!([
  {email: "beispiel@gruni.me", password: "beispiel"}
])
Admin.create!([
  {email: "gruni@gruni.me", password: "meinblog"}
])
Resume.create!([
  {}
])
Category.create!([
  {name: "Persönliche Daten", resume_id: 1},
  {name: "Schulbildung und akademischer Werdegang", resume_id: 1},
  {name: "Beruflicher Werdegang", resume_id: 1},
  {name: "Auslandsaufenthalte", resume_id: 1},
  {name: "EDV-Kenntnisse", resume_id: 1},
  {name: "Sprachkenntnisse", resume_id: 1}
])
Subcategory.create!([
  {category_id: 1, left: "Name", right: "Tobias Grunwald"},
  {category_id: 1, left: "Adresse", right: "Im Ellig 35 <br />53545 Linz am Rhein"},
  {category_id: 1, left: "Telefon", right: "0163 2942282"},
  {category_id: 1, left: "Email", right: "Tobiasgrunwald92@gmail.com"},
  {category_id: 1, left: "Geburtsdatum", right: "17.09.1992"},
  {category_id: 1, left: "Familienstand", right: "ledig"},
  {category_id: 2, left: "01.10.2012 - 29.12.2015", right: "<b>Hochschule für Telekommunikation Leipzig</b><br />Duales Studium der Wirtschaftsinformatik    <br />Abschluss: Bachelor of Science (Durchschnitt: 2,1)"},
  {category_id: 2, left: "2009 - 2012", right: "<b>Siebengebirgsgymnasium Bad Honnef</b><br />Abschluss: Allgemeine Hochschulreife"},
  {category_id: 3, left: "01.01.2014 - 29.12.2015", right: "<b>Deutsche Telekom AG, \r\nTerminal Integration & Validation</b><br><br><u>Verantwortlichkeiten:</u><ul><li>Entwicklung einer clientbasierten Testautomatisierung für den Messaging-Service Rich Communication Suite in Ruby.</li>\r\n<li>Transformation des Clients in ein Web Frontend in Ruby on Rails, HTML, CSS und Javascript.</li>\r\n<li>Machbarkeitsstudie einer Testautomatisierungssoftware und Programmierung definierter Testfälle in Java im Rahmen meiner Bachelorarbeit.</li>"},
  {category_id: 3, left: "01.10.2012 - 31.12.2013", right: "<b>Deutsche Telekom Accounting GmbH, \r\nStrategic Data Management</b><br>\r\nVerantwortlichkeiten:<ul><li>Kommunikation mit den Fachbereichen.</li><li>Pflege von Excel-Dateien inklusive Programmierung in VBA.</li><li>Aufbau und Pflege des Projektinternen Wikis</li>"},
  {category_id: 3, left: "28.07.2014 – 17.10.2014", right: "<b>T-Systems Ltd. in Bristol, Vereinigtes Königreich,  \r\nAuslandspraktikum</b><br>\r\nVerantwortlichkeiten:<ul><li>Entwicklung eines Resource Dashboards in Excel/VBA.</li><li>Unterstützung im Team (u.a. Anfertigen eines Organigrammes, Pflegen des Teamkalenders, Erstellung eines Projektplans)</li>"},
  {category_id: 4, left: "03.02.2016 – 30.01.2017", right: "Work & Travel Jahr in Australien<br>(inklusive Arbeit als Lager- und Fabrikarbeiter in Sydney,     Brisbane und Perth)"},
  {category_id: 4, left: "31.01.2017 – 23.04.2017", right: "Reise durch Südostasien\r\n(Thailand, Kambodscha, Vietnam und Laos)"},
  {category_id: 5, left: "Programmierkenntnisse", right: "Java, Ruby, VBA,\r\nRuby on Rails, HTML, CSS,\r\nArbeit mit Linux, \r\nDokumentenverwaltung mit Git,\r\nGrundkenntnisse in Javascript und PHP"},
  {category_id: 5, left: "Microsoft Office", right: "Ausgezeichnete Kenntnisse in MS Excel,  \r\nsehr gute Kenntnisse in MS Word und MS Powerpoint"},
  {category_id: 6, left: "Englisch", right: "Verhandlungssicher"},
  {category_id: 6, left: "Deutsch", right: "Muttersprache"}
])
Attachment.create!([
  {name: "Abiturzeugnis", link: "https://www.dropbox.com/s/jmew6xfwd03uvz6/Tobias%20Grunwald%20-%20Abiturzeugnis%20Siebengebirgsgymnasium.pdf?dl=0"},
  {name: "Ausbildungszeugnis Deutsche Telekom", link: "https://www.dropbox.com/s/1m8m6mvchmjh9vt/Tobias%20Grunwald%20-%20Ausbildungszeugnis%20Deutsche%20Telekom.pdf?dl=0"},
  {name: "Zeugnis Bachelor", link: "https://www.dropbox.com/s/e0aukvdwnh9pw7i/Tobias%20Grunwald%20-%20Zeugnis%20Bachelor.pdf?dl=0"},
  {name: "Europass Auslandspraktikum in Bristol", link: "https://www.dropbox.com/s/6af67jph1t7o6yc/Tobias%20Grunwald%20-%20Europass.pdf?dl=0"},
  {name: "Beispiel Bewerbung Gesamt", link: "https://www.dropbox.com/s/plkjdbe4vodnznd/Tobias%20Grunwald%20-%20Beispiel%20-%20Bewerbung%20Gesamt.pdf?dl=0"}
])
Article.create!([
  {name: "beispiel@gruni.me", content: "Sehr geehrter Herr Mustermann,\r<br />\r<br />Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\r<br />\r<br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. \r<br />\r<br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. \r<br />\r<br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. \r<br />\r<br />\r<br />Mit freundlichen Grüßen\r<br />\r<br />Tobias Grunwald", resume: 1, anhaenge: "1,2,3,4", hauptanhang: "6"}
])

