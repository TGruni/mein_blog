Rails.application.routes.draw do
  resources :attachments
  resources :subcategories
  resources :categories
  resources :categories
  resources :resumes
  root 'articles#index'
  devise_for :nutzers, :skip => [:sessions]
  as :nutzer do
    get 'login' => 'devise/sessions#new', :as => :new_nutzer_session
    post 'login' => 'devise/sessions#create', :as => :nutzer_session
    delete 'logout' => 'devise/sessions#destroy', :as => :destroy_nutzer_session
  end
  devise_for :admins, :skip => [:sessions, :registrations]
  as :admin do
    get 'tglogin' => 'devise/sessions#new', :as => :new_admin_session
    post 'tglogin' => 'devise/sessions#create', :as => :admin_session
    delete 'tglogout' => 'devise/sessions#destroy', :as => :destroy_admin_session
  end
  resources :articles
  resources :pictures
end
