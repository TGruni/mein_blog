class ArticlesController < ApplicationController
  before_action :remove_html, only: [:edit]
  before_action :authenticate_admin!, except: [:index]
  
  def index
    if current_nutzer
      @articles = Article.all.where(name: current_nutzer.email)
    end
    if current_admin
      @articles = Article.all
    end
  end
  
  def new
    @article = Article.new
    @pictures = Picture.all
    @anhaenge = Attachment.all
  end
  
  def create
    @anhaenge = Attachment.all
    @pictures = Picture.all
    @article = Article.new(article_params)
    if @article.save
      redirect_to articles_path
    else
      render action: :new, id: @article.id
    end
  end
  
  def edit
    @anhaenge = Attachment.all
    #@pictures = Picture.all
  end
  
  def update
    @anhaenge = Attachment.all
    @article = Article.find(params[:id])
    if @article.update_attributes(article_params)
      redirect_to articles_path
    else
      render action: :edit, id: @article.id
    end
  end
  
  def destroy
    @article = Article.find(params[:id])
    @article.delete
    redirect_to articles_path
  end
  
private
  def article_params
    params.require(:article).permit(:name, :content, :resume, :anhaenge, :hauptanhang)
  end
  
  def remove_html
    @article = Article.find(params[:id])
    @article.content.gsub!(/<br \/>/, '')
  end
  
end
