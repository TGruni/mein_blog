class PicturesController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_s3_direct_post, only: [:new, :edit, :create, :update, :index]

  def index
    @pictures = Picture.all
    @picture = Picture.new
  end
  
  def create
    @picture = Picture.new(picture_params)
    if @picture.save
      redirect_to pictures_path
    else
      redirect_to pictures_path, :flash => { :alert => @picture.errors.full_messages.join(', ') }
    end
  end
  
  def destroy
    @picture = Picture.find(params[:id])
    @picture.delete
    redirect_to pictures_path
  end
  
private
  def picture_params
    params.fetch(:picture, {}).permit(:image)
  end

   def set_s3_direct_post
    @s3_direct_post = S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", success_action_status: '201', acl: 'public-read')
    end
  end