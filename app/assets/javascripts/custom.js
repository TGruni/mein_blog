$(document).ready(function(){
	$("#resume_button").click(function(){
		$("#resume_div").css("display", "inline");
		$("#resume_button").attr("class", "button button_secondary");
		$("#motivation_div, #attachment_div").css("display", "none");
		$("#motivation_button, #attachment_button").attr("class", "button button_primary");
	});
	$("#motivation_button").click(function(){
		$("#motivation_div").css("display", "inline");
		$("#motivation_button").attr("class", "button button_secondary");
		$("#resume_div, #attachment_div").css("display", "none");
		$("#resume_button, #attachment_button").attr("class", "button button_primary");
	});
	$("#attachment_button").click(function(){
		$("#attachment_div").css("display", "inline");
		$("#attachment_button").attr("class", "button button_secondary");
		$("#motivation_div, #resume_div").css("display", "none");
		$("#motivation_button, #resume_button").attr("class", "button button_primary");
	});
});