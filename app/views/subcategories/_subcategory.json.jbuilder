json.extract! subcategory, :id, :category_id, :left, :right, :created_at, :updated_at
json.url subcategory_url(subcategory, format: :json)
