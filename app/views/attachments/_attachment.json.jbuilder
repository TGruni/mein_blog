json.extract! attachment, :id, :name, :link, :created_at, :updated_at
json.url attachment_url(attachment, format: :json)
