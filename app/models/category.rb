class Category < ActiveRecord::Base
  belongs_to :resume
  has_many :subcategories
end
