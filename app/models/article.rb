class Article < ActiveRecord::Base
  validates :name, presence: true
  validates :content, presence: true
  before_save :text_edit
  has_and_belongs_to_many :attachments
  
  def text_edit
    add_img
    uml
    newline
  end
  
  def add_img
    self.content.gsub!(/<</, '<img src="/uploads/picture/image/')
    self.content.gsub!(/>>/, '/image.png" />')
  end
  
  def add_mailto
    email = self.content[/<mail.*mail>/]
    email.gsub!(/<mail/, '')
    email.gsub!(/mail>/, '')
    self.content.gsub!(/<mail.*mail>/, '<a href="mailto:' + email + '">' + email + '</a>')
  end
  
  def newline
    self.content.gsub!(/\n/, '<br />')
  end
  
  def uml
    [self.name, self.content].each do |param|
      param.gsub!(/,,a/, 'ä')
      param.gsub!(/,,A/, 'Ä')
      param.gsub!(/,,o/, 'ö')
      param.gsub!(/,,O/, 'Ö')
      param.gsub!(/,,u/, 'ü')
      param.gsub!(/,,U/, 'Ü')
      param.gsub!(/,,s/, 'ß')
    end
  end
end