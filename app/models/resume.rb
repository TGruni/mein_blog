class Resume < ActiveRecord::Base
  has_many :categories
  has_many :subcategories
  has_many :nutzers
end
